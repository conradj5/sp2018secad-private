import java.net.*;
import java.io.*;
import java.util.*;

public class EchoServer {
    public static void main(String[] args) throws IOException {
        
        if (args.length != 1) {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }
        
        int portNumber = Integer.parseInt(args[0]);
        
        try {
            ServerSocket serverSocket = new ServerSocket(portNumber);
            System.out.println("EchoServer is running at port " + portNumber);
            while(true) {
                Socket clientSocket = serverSocket.accept(); 
                System.out.println("A client has connected ");    
                new EchoServerThread(clientSocket).start();
            }
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
}

class EchoServerThread extends Thread {
	private Socket clientSocket = null;
	public EchoServerThread(Socket socket){
		clientSocket = socket;
	}
	public void run(){
        try {
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);                   
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                System.out.println("received from client: " + inputLine);
                System.out.println("Echo back");
                out.println(inputLine);
            }
        } catch (IOException e){
            System.err.println ("Exception caut when listening to connection");
        }
        
	}
}

class ThreadList {
    private ArrayList<EchoServerThread> threadlist;
    public ThreadList(){
        threadlist = new ArrayList<EchoServerThread>();  
    }
    public synchronized int getNumberofThreads(){
        return threadlist.size();
    }
    public synchronized void addThread(EchoServerThread newthread){
        threadlist.add(newthread);
    }
    public synchronized void removeThread(EchoServerThread thread){
        threadlist.remove(thread);
    }
    public synchronized void sendToAll(String message){
    //ask each thread in the threadlist to send the given message to its client     
    }
}