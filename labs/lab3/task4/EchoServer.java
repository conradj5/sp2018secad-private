import java.net.*;
import java.io.*;
import java.util.ArrayList;

public class EchoServer {
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }
        int portNumber = Integer.parseInt(args[0]);
        ThreadList threadlist = new ThreadList();
        try {
            ServerSocket serverSocket = new ServerSocket(portNumber);
            System.out.println("Jonathan Conrad's EchoServer is running at port " + portNumber);
            while(true) {
                Socket clientSocket = serverSocket.accept(); 
                System.out.println("A client has connected ");    
                EchoServerThread newthread = new EchoServerThread(threadlist, clientSocket);
                threadlist.addThread(newthread);
                newthread.start();
            }
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
}

class EchoServerThread extends Thread {
    private Socket clientSocket;
    private ThreadList threadlist;
    private PrintWriter out;
    public EchoServerThread(ThreadList list, Socket socket){
        clientSocket = socket;
        threadlist = list;
    }
    public void run(){
        System.out.println("Inside thread: total clients: " + threadlist.getNumberofThreads());
        try {
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String inputLine;
            while ( !clientSocket.isClosed() && (inputLine = in.readLine()) != null) {
                System.out.println("received from client: " + inputLine);
                System.out.println("Echo back");
                //out.println(inputLine);
                if(inputLine.equals("<exit>")) {
                    threadlist.sendToAll("To All: A client exists, the number of connected client:" + (threadlist.getNumberofThreads()-1));
                    threadlist.removeThread(this);
                    clientSocket.close();
                } else 
                    threadlist.sendToAll("To All <new message>:"+ inputLine);
            }
        } catch (IOException e){
            System.err.println ("Exception caught when listening to connection");
        }
    }
    public void send(String message) {
        if (out != null) out.println(message);
    }
}

class ThreadList {
    private ArrayList<EchoServerThread> threadlist;
    public ThreadList(){
        threadlist = new ArrayList<EchoServerThread>();  
    }
    public synchronized int getNumberofThreads(){
        return threadlist.size();
    }
    public synchronized void addThread(EchoServerThread newthread){
        threadlist.add(newthread);
    }
    public synchronized void removeThread(EchoServerThread thread){
        threadlist.remove(thread);
    }
    public synchronized void sendToAll(String message){
        for(EchoServerThread thread : threadlist)
            thread.send(message);
    }
}