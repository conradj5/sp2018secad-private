#include <netdb.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>

int main (int argc, char **argv) {
	if(argc != 2) {
		fprintf(stderr, "Usage: %s <url>\n", argv[0]);
		exit(0);
	}
	printf("This is a simple TCP client application developed by Jonathan Conrad for Lab 2 in Secure Application Development - Spring \n");
	printf("URL='%s'\n", argv[1]);
	/* parse URL */
	char host[100];
	bzero(host, 100);
	char page[100];
	bzero(page, 100);
	int port = 80;
	if(sscanf((const char*)argv[1], "http://%99[^/]%99[^\n]", host, page) != 2) {
		perror("Error parsing URL");
		exit(1);
	}
	printf("Host='%s'\nPage='%s'\n", host, page);
	/* create socket and store file descriptor */
	int sockfd;
	if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("failed to create socket");
		exit(1);
	}
	/* get IP from host name */
	struct hostent *server_he;
	if ((server_he = gethostbyname(host)) == 0){
		perror("Failed to get IP of servername");
		exit(1);
	}
	/* copy h_addr into server address and set port number */
	struct sockaddr_in serveraddr;
	bzero((char*) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	bcopy((char*)server_he->h_addr, (char*)&serveraddr.sin_addr.s_addr, server_he->h_length);
	serveraddr.sin_port = htons(port);
	/* connect to IP and port */
	if (connect(sockfd, (struct sockaddr*)&serveraddr, sizeof(serveraddr)) < 0) {
		perror("Cannot connect to the server");
		exit(1);
	}
	printf("Connected to '%s' (%s) at port %d\n", host, (char*)inet_ntoa(serveraddr.sin_addr), port);
	/* get input from user and send to socket */
	char buffer[1024];
	bzero(buffer, 1024);
	snprintf(buffer, 1024, "GET %s HTTP/1.0\r\nHost: %s\r\n\r\n", page, host);
	send(sockfd, buffer, strlen(buffer), 0);
	/* recv bytes back from server */
	bzero(buffer, 1024);
	int count = 0;
	size_t size = 1024;
	char* resp = malloc(sizeof(char)*size);
	bzero(resp, 1024);
	printf("Message received:\n");
	while((count = recv(sockfd, buffer, 1024, 0)) > 0) {
		printf("%s", buffer);
		strcat(resp, buffer);
		bzero(buffer, 1024);
		resp = realloc(resp, sizeof(char)*size+1024);
		bzero(resp+size, 1024);
		size += 1024;
	}
	if (count == -1)
		perror("ERROR reading from socket");
	//printf("Response:\n%s\n", resp);
	printf("\nSize of response: %d\n", strlen(resp));
	/* must close TCP socket */
	close(sockfd);
	free(resp);
}
