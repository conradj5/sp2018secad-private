#include <netdb.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>

int main (int argc, char **argv)
{
	if(argc != 3 || atoi(argv[2]) == 0) {
		fprintf(stderr, "Usage: %s <servername> <port>\n", argv[0]);
		exit(0);
	}
	printf("This is a simple TCP client application developed by Jonathan Conrad for Lab 2 in Secure Application Development - Spring \n");
	printf("Servername='%s', Port='%s'\n", argv[1], argv[2]);

	char* servername = argv[1];
	short port = atoi(argv[2]);
	/* create socket */
	int sockfd;
	if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("failed to create socket");
		exit(1);
	}
	/* get IP from host name */
	struct hostent *server_he = gethostbyname(servername);
	if (server_he == 0){
		perror("Failed to get IP of servername");
		exit(1);
	}
	/* copy h_addr into server address and set port number */
	struct sockaddr_in serveraddr;
	bzero((char*) &serveraddr, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	bcopy((char*)server_he->h_addr, (char*)&serveraddr.sin_addr.s_addr, server_he->h_length);
	serveraddr.sin_port = htons(port);
	/* connect to IP and port */
	if (connect(sockfd, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0) {
		perror("Cannot connect to the server");
		exit(0);
	} 
	printf("Connected to '%s' (%s) at port %d\n", servername, (char*)inet_ntoa(serveraddr.sin_addr), port);
	/* get input from user and send to socket */
	char buffer[1024];
	bzero(buffer, 1024);
	snprintf(buffer, 1024, "GET / HTTP/1.0\r\nHost: %s\r\n\r\n", servername);
	send(sockfd, buffer, strlen(buffer), 0);
	/* recv bytes back from server */
	bzero(buffer, 1024);
	if(recv(sockfd, buffer, 1024, 0) < 0) 
		perror("ERROR reading from socket");
	printf("Message received: %s", buffer);
	/* must close TCP socket */
	close(sockfd);
}
