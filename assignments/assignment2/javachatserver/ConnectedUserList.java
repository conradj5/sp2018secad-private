package javachatserver;

import java.util.HashMap;

public class ConnectedUserList {
    
    private final HashMap<String, Integer> UserList;
    
    public ConnectedUserList(){
        this.UserList = new HashMap<String, Integer>();
    }
    
    synchronized public void addUser(String userName, int hash){
        this.UserList.put(userName, hash);
    }
    
    synchronized public boolean userExists(String userName){
        return this.UserList.containsKey(userName);
    }
    
    synchronized public boolean checkPassword(String userName, int hash){
        return this.UserList.get(userName) == hash;
    }
    
}