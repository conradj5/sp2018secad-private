package javachatserver;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    public static void main(String[] args) throws IOException {
        // check for valid arguments
        if (args.length != 1 || !args[0].matches("\\d+")) {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }
        int portNumber = Integer.parseInt(args[0]);
        // create threadList and userList to share with all clients
        ThreadList threadList = new ThreadList();
        ConnectedUserList userList = new ConnectedUserList();
        try {
            ServerSocket serverSocket = new ServerSocket(portNumber);
            System.out.println("Jonathan Conrad's EchoServer is running at port " + portNumber);
            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("A client has connected - " + clientSocket.getRemoteSocketAddress().toString());
                ChatServerThread newthread = new ChatServerThread(userList, threadList, clientSocket);
                newthread.start();
            }
        } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                    + portNumber);
            System.out.println(e.getMessage());
        }
    }
}
