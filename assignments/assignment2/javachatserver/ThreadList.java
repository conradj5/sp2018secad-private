package javachatserver;

import java.util.HashMap;

public class ThreadList {
    private final HashMap<String, ChatServerThread> ThreadList;
    
    public ThreadList(){
        this.ThreadList = new HashMap<String, ChatServerThread>();  
    }
    public synchronized int getNumberofThreads(){
        return this.ThreadList.size();
    }
    public synchronized void addThread(String userName, ChatServerThread newthread){
        this.ThreadList.put(userName, newthread);
    }
    public synchronized boolean userConnected(String userName){
        return this.ThreadList.containsKey(userName);
    }
    public synchronized void sendToUser(String userName, String message){
        this.ThreadList.get(userName).send(message);
    }
    public synchronized String getConnectedUsers(){
        return this.ThreadList.keySet().toString();
    }
    public synchronized void removeThread(String userName){
        if(this.ThreadList.containsKey(userName))
            this.ThreadList.remove(userName);
    }
    public synchronized void sendToAll(String message){
        for(ChatServerThread thread : this.ThreadList.values())
            thread.send(message);
    }
}