package javachatserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class ChatServerThread extends Thread {
    private final Socket clientSocket;
    private final ThreadList threadList;
    private final ConnectedUserList userList;
    private PrintWriter out;
    private String clientUserName;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("[MMM-d hh:mm:ss] ");
    private static final Timestamp timestamp = new Timestamp(0);


    
    public ChatServerThread(ConnectedUserList userList, ThreadList threadList, Socket socket){
        this.userList = userList;
        this.clientSocket = socket;
        this.threadList = threadList;
    }
    @Override
    public void run(){
        try {
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            String inputLine = null;
            // first line must be <LOGIN> command
            send("Please log in using command <LOGIN>UserName:Password");
            if(!clientSocket.isClosed() && (inputLine = in.readLine()) != null){
                String command = getCommand(inputLine);
                String[] args = getContent(inputLine).split(":");
                if(!command.equals("LOGIN") || args.length != 2 || args[0].length() < 1 || args[1].length() < 1){
                    close("Invalid command " + command + " - could not log in");
                    return;
                }
                if(threadList.userConnected(args[0])){
                    close("User " + args[0] + " is already connected");
                    return;
                }
                if(userList.userExists(args[0])){
                    if(!userList.checkPassword(args[0], args[1].hashCode())){
                        close("Invalid password for user " + args[0] + " - could not log in");
                        return;
                    }
                } else {
                    System.out.println("Adding new user " + args[0]);
                    userList.addUser(args[0], args[1].hashCode());
                }
                this.clientUserName = args[0];
                send("User " + this.clientUserName + " signed in");
                threadList.addThread(this.clientUserName, this);
            }
            
            while ( !clientSocket.isClosed() && (inputLine = in.readLine()) != null) {
                System.out.println("received from client " + clientUserName + ": " + inputLine);
                String command = getCommand(inputLine);
                String content = getContent(inputLine);
                switch(command){
                    case "LIST":
                        send("<USERS> " + this.threadList.getConnectedUsers());
                        break;
                    case "ALL":
                        threadList.sendToAll("<GLOBAL - " + this.clientUserName + "> " + content);
                        break;
                    case "MSG":
                        String[] args = content.split(":");
                        if(args.length != 2 || args[0].length() < 1 || args[1].length() < 1){
                            send("Invalid message content - should be <MSG>username:message");
                        } else if(this.threadList.userConnected(args[0])){
                            this.threadList.sendToUser(args[0], "<DM - " + this.clientUserName + "> " + args[1]);
                        } else {
                            send("User " + args[0] + " is not connected");
                        }
                        break;
                    case "EXIT":
                        close("User " + this.clientUserName + " has signed out");
                        break;
                    default:
                        send("Invalid command '" + inputLine + "'\nValid commands: <LIST> | <ALL>Message | <MSG>username:Message | <EXIT>");
                        break;
                }
            }
            // make sure thread removed from list
            threadList.removeThread(this.clientUserName);
            System.out.println("client " + this.clientUserName + " removed " + this.threadList.getConnectedUsers());
        } catch (IOException e){
            System.err.println ("Exception caught when listening to connection");
        }
    }
    public void send(String message) {
        timestamp.setTime(System.currentTimeMillis());
        if (out != null) out.println(sdf.format(timestamp) + message);
    }
    
    private void close(String message) throws IOException{
        send(message);
        threadList.removeThread(this.clientUserName);
        clientSocket.close();
    }
    
    // return the given command
    private String getCommand(String line){
        // make sure it contains a potential command
        if(line.startsWith("<") && line.substring(1).contains(">"))
            return line.substring(line.indexOf("<")+1, line.indexOf(">"));
        return "";
    }
    // return the content of the message
    private String getContent(String line){
        if(line.startsWith("<") && line.substring(1).contains(">"))
            return line.substring(line.indexOf(">")+1);
        return "";
    }
}