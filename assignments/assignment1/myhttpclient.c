#include <netdb.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>

#define SIZE 100
#define REQ_SIZE 1024
#define BUFF_SIZE 1024*1024


int main (int argc, char **argv) {
	// check args
	if(argc != 2 || !argv[1] || strlen(argv[1]) > 200) {
		fprintf(stderr, "Usage: %s <url>\n", argv[0]);
		exit(0);
	}
	printf("\nThis is a simple TCP client application developed by Jonathan Conrad \n   for Lab 2 in Secure Application Development - Spring 2018\n\n");

	/* 1) Handle the input to get hostname and path (and file name) */
	char host[SIZE], page[SIZE], fname[SIZE];
	bzero(host, SIZE);
	bzero(page, SIZE);
	bzero(fname, SIZE);
	int port = 80;
	// parse host and path
	if(sscanf((const char*)argv[1], "http://%99[^/]%99[^\n]", host, page) != 2) {
		perror("Error parsing URL");
		exit(1);
	}
	// parse file name from url
	if(page[strlen(page)-1] == '/')
		strncpy(fname, "index.html", SIZE);
	else
		strncpy(fname, strrchr(page, '/') + 1, SIZE);
	// debug variables
	printf("Url = %s\nHost = '%s'\nPage = '%s'\nFile Name = '%s'\n", argv[1], host, page, fname);
	
	/* 2) Establish a connection to the server */
	// create socket and store file descriptor
	int sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if(sockfd < 0) {
		perror("Failed to create socket");
		exit(1);
	}
	// get IP from host name using getaddrinfo
	struct addrinfo hints, *res;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	if (getaddrinfo(host, NULL, &hints, &res) != 0 || !res) {
		perror("Error in getaddrinfo");
		exit(2);
	}
	// save pointer to address stored in res and set family and port
	struct sockaddr_in* serveraddr = (struct sockaddr_in *)res->ai_addr;
	serveraddr->sin_family = AF_INET;
	serveraddr->sin_port = htons(port);
	// connect to IP and port
	if (connect(sockfd, (struct sockaddr *)serveraddr, sizeof(struct sockaddr)) < 0) {
		perror("Cannot connect to the server");
		exit(1);
	}
	// store address as char* and print
	char addr[INET_ADDRSTRLEN];
	inet_ntop(AF_INET, &(serveraddr->sin_addr), addr, INET_ADDRSTRLEN);
	printf("Connected to '%s' (%s) at port %d\n", host, addr, port);
	
	/* 3) Construct a correct HTTP request and send to the server */
	char get_req[REQ_SIZE];
	bzero(get_req, REQ_SIZE);
	snprintf(get_req, REQ_SIZE, "GET %s HTTP/1.0\r\nHost: %s\r\n\r\n", page, host);
	send(sockfd, get_req, strlen(get_req), 0);
	// recv response back from server
	char buffer[BUFF_SIZE];
	bzero(buffer, BUFF_SIZE);
	int bytes_received = recv(sockfd, buffer, BUFF_SIZE, 0);
	if(bytes_received < 0) {
		perror("Error reading from socket");
		exit(1);
	}

	/* 4) Handle HTTP response code */
	int resp_code = 0;
	if(sscanf((const char*)buffer, "HTTP/1.%*c %d OK", &resp_code) != 1) {
		perror("Error parsing response code");
		exit(1);
	}
	printf("Response code %d\n", resp_code);
	if(resp_code == 200){
		/* 5) Store data to a corresponding file */
		printf("Writing response to file: %s\n", fname);
		FILE* fp = fopen(fname, "w");
		// write data after the headers
		char* end_of_header = strstr(buffer, "\r\n\r\n");
		int header_size = end_of_header - buffer;
		int body_len = bytes_received - (header_size + 4);
		fwrite(end_of_header+4, body_len, 1, fp);
		bzero(buffer, BUFF_SIZE);
		// continue recving from socket and writing to file
		while((bytes_received = recv(sockfd, buffer, BUFF_SIZE, 0)) > 0) {
			fwrite(buffer, bytes_received, 1, fp);
			bzero(buffer, BUFF_SIZE);
		}
		if (bytes_received < 0) {
			perror("ERROR reading from socket");
		}
		/* close file */
		fclose(fp);
	} 
	/* close TCP socket */
	close(sockfd);
}