<?php
  //Security principle: Never use the root database account in the web application
  $mysqli = new mysqli('localhost', 'sp2018secad', /*Database username*/
                                    'cd066b3e2e',  /*Database password*/
                                    'sp2018secad'  /*Database name*/);
  //echo "mysql";
  if ($mysqli->connect_error)
      die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);

  function mysql_checklogin_secure ($username, $password) {
    global $mysqli;
    $prepared_sql = "SELECT * FROM users where username= ?"." and password=password(?);";
    if(!$stmt = $mysqli->prepare($prepared_sql)){
      echo "Prepared Statement Error";
      return FALSE;
    }
    $stmt->bind_param("ss", $username, $password);
    if(!$stmt->execute()) { echo "Execute Error"; return FALSE; }
    if(!$stmt->store_result()) { echo "Store_result Error"; return FALSE; }
    return $stmt->num_rows == 1;
  }
  function mysql_change_users_password($newpassword) {
    global $mysqli;
    $prepared_sql = "UPDATE users SET password=password(?) WHERE username= ?;";
    if(!$stmt = $mysqli->prepare($prepared_sql)){
      echo "Prepared Statement Error";
      return FALSE;
    }
    $stmt->bind_param("ss", $newpassword, $_SESSION["username"]);
    if(!$stmt->execute()) {
      echo "Execute Error";
      return FALSE;
    }
    return TRUE;
  }
  function show_posts() {
    //debug: echo "->show_posts: retreive the posts from database and display<br>";
    global $mysqli;
    $sql = "SELECT * FROM posts";
    $result = $mysqli->query($sql);
    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        $postid = $row["id"];
        echo "<h3>Post " . $postid . " - " . $row["title"]. " </h3>";
        echo $row["text"] . "<br>";
        echo "<a href='comment.php?postid=$postid'>";
        $sql = "SELECT * FROM comments WHERE postid='$postid';";
        $comments = $mysqli->query($sql);
        if ($comments->num_rows > 0) {
          echo $comments->num_rows . " comments </a>";
        } else{
          echo "Post the first comment </a>";
        }
      }
    } else { echo "No post in this blog yet <br>"; }
  }
  function display_singlepost($id) {
    //debug: echo "->show_posts: retreive the posts from database and display<br>";
    global $mysqli;
    $sql = "SELECT * FROM posts WHERE id='$id'";
    $result = $mysqli->query($sql);
    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        $postid = $row["id"];
        echo "<h3>Post " . $postid . " - " . $row["title"]. " </h3>";
        echo $row["text"] . "<br>";
        $sql = "SELECT * FROM comments WHERE postid='$postid';";
        $comments = $mysqli->query($sql);
        if ($comments->num_rows > 0) {
          echo $comments->num_rows . " comments </a>";
        }
      }
    } else { echo "No post in this blog yet <br>"; }
  }

  function show_post_table() {
    //debug: echo "->show_posts: retreive the posts from database and display<br>";
    global $mysqli;
    $sql = "SELECT * FROM posts";
    $result = $mysqli->query($sql);
    if ($result->num_rows > 0) {
      // output data of each row
      echo "<style>table, th, td {border: 1px solid black;}</style><br>";
      echo "<table>";
      while($row = $result->fetch_assoc()) {
        $postid = $row["id"];
        echo "<tr>";
        echo "<td>Post " . $postid . " - " . $row["title"]. " </td>";
        echo "<td><a href='newpost.php?postid=$postid'>edit</a></td>";
        echo "<td><a href='admin.php?delete=$postid'>delete</a></td>";
        echo "</tr>";
      }
      echo "</table>";
    } else { echo "No post in this blog yet <br>"; }
  }

  function get_post($postid){
    global $mysqli;
    $sql = "SELECT * FROM posts WHERE id=$postid;";
    $result = $mysqli->query($sql);
    $row = $result->fetch_assoc();
    $title = $row['title'];
    $text = $row['text'];
    //this will bind each row with the variables
    return array($title, $text);
  }

  function get_post2($postid) {
    global $mysqli;
    $prepared_sql = "SELECT title, text FROM posts WHERE id=?;";
    if(!$stmt = $mysqli->prepare($prepared_sql))
      echo "Prepared Statement Error";
    $stmt->bind_param('i', $postid);
    if(!$stmt->execute()) echo "Execute failed";
    $title = NULL;
    $content = NULL;
    if(!$stmt->bind_result($title, $content)) echo "Binding failed ";
    $stmt->fetch();
    return array($title, $content);
  }

  function display_comments($postid){
    global $mysqli;
    echo "Comments for Postid= $postid <br>";
    $prepared_sql = "select title, content from comments where postid=?;";
    if(!$stmt = $mysqli->prepare($prepared_sql))
      echo "Prepared Statement Error";

    $stmt->bind_param('i', $postid);
    if(!$stmt->execute()) echo "Execute failed";
    $title = NULL;
    $content = NULL;
    if(!$stmt->bind_result($title, $content)) echo "Binding failed ";
    //this will bind each row with the variables
    $num_rows = 0;
    while($stmt->fetch()){
      echo "Comment title:" . htmlentities($title) . "<br>";
      echo htmlentities($content) . "<br>";
      $num_rows++;
    }

    if($num_rows==0) echo "No comment for this post. Please post your comment";
  }

  function new_comment($postid, $tit1e, $content, $commenter){

    global $mysqli;

    $prepared_sql = "INSERT into comments (title,content,commenter,postid) VALUES (?,?,?,?);";

    if(!$stmt = $mysqli->prepare($prepared_sql))
      echo "Prepared Statement Error";

    $stmt->bind_param("sssi", htmlspecialchars($tit1e),
      htmlspecialchars($content),
      htmlspecialchars($commenter), 
      $postid);
    if(!$stmt->execute()) { echo "Execute Error"; return FALSE; }
    return TRUE;
  }

  function remove_post($postid) {
    global $mysqli;
    $sql = "DELETE FROM posts WHERE id=$postid;";
    $mysqli->query($sql);
  }
?>
