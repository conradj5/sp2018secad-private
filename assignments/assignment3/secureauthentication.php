<?php
  //  Jonathan Conrad
  //ini_set("session.cookie_httponly", TRUE);
  //ini_set("session.cookie_secure", TRUE);
  session_start();
  require 'mysql.php';
  // implement session timeout
  if (!isset($_SESSION['time'])) {
    $_SESSION['time'] = time();
  } else if (time() - $_SESSION['time'] > 900) {
    // session started more than 15 minutes ago
    session_regenerate_id(TRUE); // create new session
    $_SESSION['time'] = time();  // update creation time
  }
  if (!isset($_SESSION['browser'])){
    $_SESSION['browser'] = $_SERVER['HTTP_USER_AGENT'];
  } else if ($_SESSION["browser"] != $_SERVER["HTTP_USER_AGENT"]){
    echo "<script>alert('Session hijacking detected.');</script>";
    die();
  }
  if (isset($_POST["username"]) and isset($_POST["password"]) ){
    if (mysql_checklogin_secure($_POST["username"],$_POST["password"])) {
      $_SESSION["logged"] = TRUE;
      $_SESSION["username"] = $_POST["username"];
      $_SESSION["browser"] = $_SERVER["HTTP_USER_AGENT"];
    } else {
       echo "<script>alert('Invalid username/password');</script>";
       unset($_SESSION["logged"]); 
    }
  }
  if (!isset($_SESSION["logged"]) or $_SESSION["logged"] != TRUE) {
    echo "<script>alert('You have not logged in. Please logged in.');</script>";
    header("Refresh:0; url=form.php");
    die();
  }
?>
