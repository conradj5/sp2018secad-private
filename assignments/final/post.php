<?php 
	require "secureauthentication.php";
	include "header.php";
	$title = $_POST['title'];
	$text = $_POST['text'];
	$nocsrftoken = $_POST['nocsrftoken'];
	$sessionnocsrftoken = $_SESSION["nocsrftoken"];

	if (isset($title) and isset($text)){
		if (!isset($nocsrftoken) or ($nocsrftoken != $sessionnocsrftoken)){
			echo "cross site request forgery is detected";
			session_destroy();
			die();
		}
		if($_POST['updateid'] != '') {
			if (update_post($title, $text, $_POST['updateid'])) {
				echo "Edited post";
			} else {
				echo "Cannot edit post";
			}
		} else {
			if (new_post($title, $text)){
				echo "added new post";
			} else {
				echo "cannot add the new post";
			}
		}	
		header('Location: home.php');
	}

	$post_info = array('', '');
	if(isset($_REQUEST['postid'])) {
		$post_info = get_post($_REQUEST['postid']);
	}


?>

<form action="post.php?postid=<?php echo $_REQUEST['postid']?>" method="POST" class="form login">
	<?php
		$rand = bin2hex(openssl_random_pseudo_bytes(16));
		$_SESSION["nocsrftoken"] = $rand;
	?>
	<div>
		<input type="hidden" name="updateid" value="<?php echo $_REQUEST['postid']; ?>"/>
		<input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>"/>
		Title: <input type="text" name="title" value="<?php echo $post_info[0]?>" required/><br>
		Content: <br><textarea name="text" required cols="100" rows="10"><?php echo $post_info[1]?></textarea><br>
		<button class="button" type="submit">Submit</button>
	</div>
</form>
</html>
