<html>
	<script>
		function send(type, post) {
			var http = new XMLHttpRequest();
			var url = "home.php";
			var params = type + "=" + post + "&token=" + document.getElementById('token').value;
			http.open("POST", url, true);
			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			http.onreadystatechange = function() {//Call a function when the state changes.
				if(http.readyState == 4 && http.status == 200) {
					document.body.innerHTML = http.response;
				}
			}
			http.send(params);
		}
	</script>
	<?php
		require 'secureauthentication.php';
		include "header.php";
		if(isset($_POST['delete'])) {
			echo "<script>if(!confirm('Are you sure you want to remove this post?')){document.location = 'home.php';}</script>";
			remove_post($_POST['delete']);
		}
		if(isset($_POST['enable'])) {
			enable_post($_POST['enable']);
				//header('Location: home.php');
		}

	?>
	<input type="hidden" id="token" name="token" value="<?php echo $rand?>" />
	<h1> Welcome <?php echo htmlspecialchars($_SESSION["username"])?> </h1>
	<h3> Manage your profile and your posts from here. </h3>

	<div> <a href="post.php">New Post</a> </div>
	<?php
		show_post_table();
	?>
</html>