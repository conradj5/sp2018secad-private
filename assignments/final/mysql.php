<?php
  //Security principle: Never use the root database account in the web application
  $mysqli = new mysqli('localhost', 'sp2018secad', /*Database username*/
                                    'cd066b3e2e',  /*Database password*/
                                    'finalproject'  /*Database name*/);
  //echo "mysql";
  if ($mysqli->connect_error)
      die('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);

  function mysql_checklogin_secure ($username, $password) {
    global $mysqli;
    $prepared_sql = "SELECT * FROM users WHERE username= ? AND password=password(?) AND approved=1;";
    if(!$stmt = $mysqli->prepare($prepared_sql)) return FALSE;
    if(!$stmt->bind_param("ss", $username, $password)) return FALSE;
    if(!$stmt->execute()) { echo "Execute Error"; return FALSE; }
    if(!$stmt->store_result()) { echo "Store_result Error"; return FALSE; }
    return $stmt->num_rows == 1;
  }

  function get_user_info() {
    global $mysqli;
    $prepared_sql = "SELECT name, email, phone FROM users WHERE username= ?;";
    if(!$stmt = $mysqli->prepare($prepared_sql))
      echo "Prepared Statement Error";
    if(!$stmt->bind_param('s', $_SESSION['username'])) echo "Binding param failed";
    if(!$stmt->execute()) echo "Execute failed";
    if(!$stmt->bind_result($name, $email, $phone)) echo "Binding failed ";
    $stmt->fetch();
    return array(htmlspecialchars($name), htmlspecialchars($email), htmlspecialchars($phone));
  }

  function is_admin() {
    global $mysqli;
    $prepared_sql = "SELECT admin FROM users WHERE username= ?;";
    if(!$stmt = $mysqli->prepare($prepared_sql))
      echo "Prepared Statement Error";
    if(!$stmt->bind_param('s', $_SESSION['username'])) echo "Binding param failed";
    if(!$stmt->execute()) echo "Execute failed";
    if(!$stmt->bind_result($admin)) echo "Binding failed ";
    $stmt->fetch();
    return $admin;
  }

  function create_user($data) {
    global $mysqli;
    $prepared_sql = "INSERT into users (username, password, name, email, phone) VALUES (?, password(?), ?, ?, ?);";
    if(!$stmt = $mysqli->prepare($prepared_sql)){
      echo "Preapred Statement Error New";
      return FALSE;
    }
    if(!$stmt->bind_param("sssss", htmlspecialchars($data['username']), htmlspecialchars($data['password']), 
                      htmlspecialchars($data['name']), htmlspecialchars($data['email']),
                      htmlspecialchars($data['phone']))) {
      echo "Bind error";
      return FALSE;
    }
    if(!$stmt->execute()) {
       echo "Execute error";
       return FALSE;
    }
    return mysqli_affected_rows($mysqli) == 1;
  }

  function update_user_info($name, $email, $phone) {
    global $mysqli;
    $prepared_sql = "UPDATE users SET name=?, email=?, phone=? WHERE username=?";
    if(!$stmt = $mysqli->prepare($prepared_sql)) {
       echo "Preapred Statement Error Update";
    }
    if(!$stmt->bind_param("ssss", htmlspecialchars($name), htmlspecialchars($email), 
                              htmlspecialchars($phone), $_SESSION['username']))
      return FALSE;
    if(!$stmt->execute()) { echo "Execute error"; return FALSE; }
    return mysqli_affected_rows($mysqli) == 1;
  }

  function show_approve_account_table() {
    global $mysqli;
    $sql = "SELECT * FROM users WHERE approved=0";
    $result = $mysqli->query($sql);
    if ($result->num_rows > 0) {
      // output data of each row
      echo "<style>table, th, td {border: 1px solid black;}</style><br>";
      echo "<table><tr><th style=\"min-width: 100px\">Account</th><th>Status</th></tr>";
      while($row = $result->fetch_assoc()) {
        $username = htmlspecialchars($row['username']);
        echo "<tr>";
        echo "<td style=\"min-width: 100px\">{$row['username']}</td>";
        echo "<td><a href=\"javascript:enable('$username');\">approve</a></td>";
        echo "</tr>";
      }
      echo "</table>";
    } else { echo "No accounts need approval.<br>"; }
  }

  function show_account_table() {
    global $mysqli;
    $sql = "SELECT * FROM users";
    $result = $mysqli->query($sql);
    if ($result->num_rows > 0) {
      // output data of each row
      echo "<style>table, th, td {border: 1px solid black;}</style>";
      echo "<table><tr><th style=\"min-width: 100px\">Account</th><th>Status</th></tr>";
      while($row = $result->fetch_assoc()) {
        $username = htmlspecialchars($row['username']);
        echo "<tr>";
        echo "<td>$username</td>";
        //echo "<td><a href='post.php?postid=$postid'>edit</a></td>";
        echo '<td><label>enabled<input type="checkbox"' . ($row['approved']?' checked ':"");
        echo "onchange=\"enable('$username');\"><span class=\"checkmark\"></span></label></td>";
        echo "</tr>";
      }
      echo "</table>";
    } else { echo "No users in the database <br>"; }
  }

  function enable_account($user) {
    global $mysqli;
    if(!is_admin()) return FALSE;
    $prepared_sql = "UPDATE users SET approved=NOT approved WHERE username=? AND admin=0;";
    if(!$stmt = $mysqli->prepare($prepared_sql)){
      echo "Prepared Statement Error";
      return FALSE;
    }
    if(!$stmt->bind_param("s", $user))
      return FALSE;
    if(!$stmt->execute()) {
      echo "Execute Error";
      return FALSE;
    }
    return mysqli_affected_rows($mysqli) == 1;
  }

  function change_password($newpassword) {
    global $mysqli;
    $prepared_sql = "UPDATE users SET password=password(?) WHERE username= ?;";
    if(!$stmt = $mysqli->prepare($prepared_sql)){
      echo "Prepared Statement Error";
      return FALSE;
    }
    if(!$stmt->bind_param("ss", $newpassword, $_SESSION["username"]))
      return FALSE;
    if(!$stmt->execute()) {
      echo "Execute Error";
      return FALSE;
    }
    return TRUE;
  }

  function show_posts() {
    global $mysqli;
    $sql = "SELECT * FROM posts WHERE enabled=1";
    $result = $mysqli->query($sql);
    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        $postid = $row["id"];
        echo "<h3>Post " . $postid . " - " . htmlspecialchars($row["title"]) . " </h3>";
        echo $row["text"] . "<br>";
        echo "<a href='comment.php?postid=$postid'>";
        $sql = "SELECT * FROM comments WHERE postid='$postid';";
        $comments = $mysqli->query($sql);
        if ($comments->num_rows > 0) {
          echo $comments->num_rows . " comments </a>";
        } else{
          echo "Post the first comment </a>";
        }
      }
    } else { echo "No post in this blog yet <br>"; }
  }

  function enable_post($id){
    global $mysqli;
    $prepared_sql = "UPDATE posts SET enabled=NOT enabled  WHERE owner=? AND id=?;";
    if(!$stmt = $mysqli->prepare($prepared_sql)){
      echo "Prepared Statement Error";
      return FALSE;
    }
    if(!$stmt->bind_param("si", $_SESSION["username"], $id))
      return FALSE;
    if(!$stmt->execute()) {
      echo "Execute Error";
      return FALSE;
    }
    return mysqli_affected_rows($mysqli) == 1;
  }

  function display_singlepost($id) {
    //debug: echo "->show_posts: retreive the posts from database and display<br>";
    global $mysqli;
    $sql = "SELECT * FROM posts WHERE id='$id'";
    $result = $mysqli->query($sql);
    if ($result->num_rows > 0) {
      // output data of each row
      while($row = $result->fetch_assoc()) {
        $postid = $row["id"];
        echo "<h3>Post " . htmlspecialchars($postid) . " - " . htmlspecialchars($row["title"]) . " </h3>";
        echo htmlspecialchars($row["text"]) . "<br>";
        $sql = "SELECT * FROM comments WHERE postid='$postid';";
        $comments = $mysqli->query($sql);
        if ($comments->num_rows > 0) {
          echo $comments->num_rows . " comments </a>";
        }
      }
    } else { echo "No post in this blog yet <br>"; }
  }

  function show_post_table() {
    global $mysqli;
    $username = $_SESSION['username'];
    $sql = "SELECT * FROM posts WHERE owner='$username'";
    $result = $mysqli->query($sql);
    if ($result->num_rows > 0) {
      // output data of each row
      echo "<style>table, th, td {border: 1px solid black;}</style><br>";
      echo "<table>";
      while($row = $result->fetch_assoc()) {
        $postid = $row["id"];
        echo "<tr>";
        echo "<td>Post " . $postid . " - <a href='post.php?postid=$postid'>" . htmlentities($row["title"]) . " </a></td>";
        //echo "<td><a href='post.php?postid=$postid'>edit</a></td>";
        echo "<td><a href=\"javascript:send('delete', $postid);\">delete</a></td>";
        echo '<td><label>enabled<input type="checkbox"' . ($row['enabled'] ? ' checked ' : "");
        echo "onchange=\"send('enable', '{$row['id']}');\"><span class=\"checkmark\"></span></label></td>";
        echo "</tr>";
      }
      echo "</table>";
    } else { echo "No post in this blog yet <br>"; }
  }

  function get_post($postid) {
    global $mysqli;
    $prepared_sql = "SELECT title, text FROM posts WHERE id=?;";
    if(!$stmt = $mysqli->prepare($prepared_sql))
      echo "Prepared Statement Error";
    $stmt->bind_param('i', $postid);
    if(!$stmt->execute()) echo "Execute failed";
    $title = NULL;
    $content = NULL;
    if(!$stmt->bind_result($title, $content)) echo "Binding failed ";
    $stmt->fetch();
    return array($title, $content);
  }

  function new_post($title, $text){
    global $mysqli;
    $prepared_sql = "INSERT into posts (title, text, owner) VALUES (?,?,?);";
    if(!$stmt = $mysqli->prepare($prepared_sql)){
      echo "Preapred Statement Error New";
      return FALSE;
    }
    if(!$stmt->bind_param("sss", htmlspecialchars($title), htmlspecialchars($text), 
                      htmlspecialchars($_SESSION['username']))) return FALSE;
    if(!$stmt->execute()) {
       echo "Execute error";
       return FALSE;
    }
    return mysqli_affected_rows($mysqli) == 1;
  }

  function update_post($title, $text, $id) {
    global $mysqli;
    $prepared_sql = "UPDATE posts SET title=?, text=? WHERE owner= ? and id=?;";
    if(!$stmt = $mysqli->prepare($prepared_sql)){
      echo "Preapred Statement Error Update";
    }
    $stmt->bind_param("sssi", htmlspecialchars($title), htmlspecialchars($text), 
                              htmlspecialchars($_SESSION['username']), $id);
      if(!$stmt->execute()) {
         echo "Execute error";
         return FALSE;
      }
    return TRUE;
  }

  function remove_post($postid) {
    global $mysqli;
    $sql = "DELETE FROM posts WHERE id=$postid;";
    $mysqli->query($sql);
  }

  function display_comments($postid){
    global $mysqli;
    
    $prepared_sql = "select title, content from comments where postid=?;";
    if(!$stmt = $mysqli->prepare($prepared_sql))
      echo "Prepared Statement Error";

    $stmt->bind_param('i', $postid);
    if(!$stmt->execute()) echo "Execute failed";
    $title = NULL;
    $content = NULL;
    if(!$stmt->bind_result($title, $content)) echo "Binding failed ";
    //this will bind each row with the variables
    $num_rows = 0;
    while($stmt->fetch()){
      echo "Comment title:" . htmlentities($title) . "<br>";
      echo htmlentities($content) . "<br>";
      $num_rows++;
    }

    if($num_rows==0) echo "No comment for this post. Please post your comment";
  }

  function new_comment($postid, $tit1e, $content, $commenter){

    global $mysqli;

    $prepared_sql = "INSERT into comments (title,content,commenter,postid) VALUES (?,?,?,?);";

    if(!$stmt = $mysqli->prepare($prepared_sql))
      echo "Prepared Statement Error";

    $stmt->bind_param("sssi", htmlspecialchars($tit1e),
      htmlspecialchars($content),
      htmlspecialchars($commenter), 
      $postid);
    if(!$stmt->execute()) { echo "Execute Error"; return FALSE; }
    return TRUE;
  }

?>
