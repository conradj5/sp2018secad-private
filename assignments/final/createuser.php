<?php
	session_start();
	require "mysql.php";
  	include "header.php";
	// handle update info query
	if(isset($_POST['name']) and isset($_POST['email']) and isset($_POST['phone']) and isset($_POST['password']) and isset($_POST['validate'])) {
		// check token
		if(!isset($_POST['token']) or $_POST['token'] != $_SESSION["nocsrftoken"]){
			echo "<script>alert('CSR Forgery is detected')</script>";
			session_destroy();
			die();
		}
		if($_POST['password'] != $_POST['validate']){
			echo "<p>Passwords do not match</p>";
			die();
		}
		else if(create_user($_POST)) {
			echo "<p>Successfully created profile! Please wait for approval by an admin.</p>";
      		die();
      	}
		else {
			echo "<p>Failed to create profile!</p>";
      		die();
      	}
	}

	$rand = bin2hex(openssl_random_pseudo_bytes(16));
	$_SESSION["nocsrftoken"] = $rand;
?>
<h1> Create Profile </h1>
<form action="createuser.php" method="POST" class="form editprofile">
	<input type="hidden" name="token" value="<?php echo $rand?>" />
	<table>
		<tr>
			<td>Username:</td>
			<td><input type="text" name="username"/></td>
		</tr>
		<tr>
			<td>Password:</td>
			<td><input type="password" name="password"/></td>
		</tr>
		<tr>
			<td>Password:</td>
			<td><input type="password" name="validate"/></td>
		</tr>
		<tr>
			<td>Name:</td>
			<td><input type="text" name="name"/></td>
		</tr>
		<tr>
			<td>Email:</td>
			<td><input type="email" name="email"/></td>
		</tr>
		<tr>
			<td>Phone:</td>
			<td><input type="tel" name="phone"/></td>
		</tr>
	</table>
	<br>
	<button class="button" type="submit">Create Profile</button>
</form>