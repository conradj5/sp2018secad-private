<html>
	<script>
		function enable(user) {
			var http = new XMLHttpRequest();
			var url = "admin.php";
			var params = "approve=" + user + "&token=" + document.getElementById('token').value;
			http.open("POST", url, true);
			http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			http.onreadystatechange = function() {//Call a function when the state changes.
				if(http.readyState == 4 && http.status == 200) {
					document.body.innerHTML = http.response;
				}
			}
			http.send(params);
		}
	</script>
	<?php
		require "secureauthentication.php";
		include "header.php";
		// double check the current user is an admin
		if(!is_admin()) {
			echo "<script>alert('You do not have permission to view this page.'); </script>";
			//header("Location: index.php");
			die();
		}

		if(isset($_POST['approve'])) {
			if(!isset($_POST['token']) or $_POST['token'] != $_SESSION['nocsrftoken']) {
				echo "<script>alert('CSRF detected');</script>";
				session_destroy();
				die();
			}
			enable_account($_POST['approve']);
				
		}

		$rand = bin2hex(openssl_random_pseudo_bytes(16));
		$_SESSION["nocsrftoken"] = $rand;

	?>
	<input type="hidden" id="token" name="token" value="<?php echo $rand?>" />
	<h1>Admin Dashboard</h1>
	<h3>Accounts waiting approval.</h3>
	<?php show_approve_account_table(); ?>
	<h3>All accounts in database</h3>
	<?php show_account_table(); ?>
</html>