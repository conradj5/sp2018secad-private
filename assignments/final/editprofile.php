<html>
  <?php
    require "secureauthentication.php";
    include "header.php";
    // handle update info query
    if(isset($_POST['name']) and isset($_POST['email']) and isset($_POST['phone']) and isset($_POST['pass'])) {
      // check token
      if(!isset($_POST['token']) or $_POST['token'] != $_SESSION["nocsrftoken"]){
        echo "<script>alert('CSR Forgery is detected')</script>";
        session_destroy();
        die();
      }
      if(!mysql_checklogin_secure($_SESSION['username'], $_POST['pass']))
        echo "<p>Incorrect password</p>";
      else if(update_user_info($_POST['name'], $_POST['email'], $_POST['phone']))
        echo "<p>Successfully updated profile!</p>";
      else
        echo "<p>Failed to update profile!</p>";
    }

    $rand = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["nocsrftoken"] = $rand;
    $info = get_user_info();
  ?>
  <h1> Edit Profile </h1>
  <h3>Edit profile information for '<?php echo htmlspecialchars($_SESSION["username"]) ?>'</h3>
  <form action="editprofile.php" method="POST" class="form editprofile">
    <input type="hidden" name="token" value="<?php echo $rand?>" />
    <table>
      <tr>
        <td>Name:</td>
        <td><input type="text" name="name" value="<?php echo htmlspecialchars($info[0])?>"/></td>
      </tr>
      <tr>
        <td>Email:</td>
        <td><input type="text" name="email" value="<?php echo htmlspecialchars($info[1])?>"/></td>
      </tr>
      <tr>
        <td>Phone:</td>
        <td><input type="text" name="phone" value="<?php echo htmlspecialchars($info[2])?>"/></td>
      </tr>
      <tr>
        <td>Password:</td>
        <td><input type="password" name="pass"/></td>
      </tr>
    </table>
    <br>
    <button class="button" type="submit">Update Info</button>
  </form>

</html>