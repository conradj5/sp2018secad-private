<html>
  <?php
    require "secureauthentication.php";
    include "header.php";
    $rand = bin2hex(openssl_random_pseudo_bytes(16));
    $_SESSION["nocsrftoken"] = $rand; 
  ?>
  <h1>Password Manager</h1>
  <h3>Change password for '<?php echo htmlspecialchars($_SESSION["username"])?>'</h3>
  <form action="changepassword.php" method="POST" class="form changepass">
    <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
    <table>
      <tr>
        <td>Password:</td>
        <td><input type="password" name="pass"/></td>
      </tr>
      <tr>
        <td>New Password:</td>
        <td><input type="password" name="newpassword" /></td>
      </tr>
      <tr>
        <td>Confirm Password:</td>
        <td><input type="password" name="confirm" /></td>
      </tr>
    </table>
    <br>
    <button class="button" type="submit">Change Password</button>
  </form>
</html>