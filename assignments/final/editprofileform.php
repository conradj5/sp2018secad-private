<?php
  require "secureauthentication.php";
  include "header.php";
  // handle update info query
  if(isset($_POST['name']) and isset($_POST['email']) and isset($_POST['phone'])) {
    // check token
    if(!isset($_POST['token']) or $_POST['token'] != $_SESSION["nocsrftoken"]){
      echo "<script>alert('CSR Forgery is detected')</script>";
      session_destroy();
      die();
    }
    if(update_user_info($_POST['name'], $_POST['email'], $_POST['phone']))
      echo "<p>Successfully updated profile!</p>";
      die();
    else
      echo "<p>Failed to update profile!</p>";
      die();
  }

  echo "<h3>Edit profile information for '" . htmlspecialchars($_SESSION["username"]) . "'</h3>";
  $rand = bin2hex(openssl_random_pseudo_bytes(16));
  $_SESSION["nocsrftoken"] = $rand;
  $info = get_user_info();
?>
<h1> Edit Profile </h1>
  <form action="editprofile.php" method="POST" class="form editprofile">
    <input type="hidden" name="token" value="<?php echo $rand?>" />
    <table>
      <tr>
        <td>Name:</td>
        <td><input type="text" name="name" value="<?php echo $info[0]?>"/></td>
      </tr>
      <tr>
        <td>Email:</td>
        <td><input type="text" name="email" value="<?php echo $info[1]?>"/></td>
      </tr>
      <tr>
        <td>Phone:</td>
        <td><input type="text" name="phone" value="<?php echo $info[2]?>"/></td>
      </tr>
    </table>
    <button class="button" type="submit">Update Info</button>
  </form>

  <div>
    <a href="home.php">Home</a> |
    <a href="admin.php">Admin</a> | 
    <a href="logout.php">Logout</a> 
  </div>
</html>