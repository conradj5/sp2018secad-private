<?php 
	session_start();
	include 'header.php';
	$rand = bin2hex(openssl_random_pseudo_bytes(16));
	$_SESSION["nocsrftoken"] = $rand;
?>
<h1>Login</h1>
<form action="home.php" method="POST" class="form login">
	<input type="hidden" id="token" name="token" value="<?php echo $rand?>" />
  Username:<input type="text" class="text_field" name="username" /> <br>
  Password: <input type="password" class="text_field" name="password" /> <br>
  <button class="button" type="submit">Login</button>
</form>