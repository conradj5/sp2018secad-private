<header>
	<div style="display: inline-block;">
		<a href="index.php"><h2>Jonathan's Blog</h2></a>
	</div>
	<!-- begin menue div -->
	<div style="display: inline-block; float: right">
		<?php
			if(isset($_SESSION['logged'])) {
				echo '<a href="home.php">Home</a> | ';
				echo '<a href="logout.php">Logout</a> | ';
				echo '<a href="editprofile.php">Edit Profile</a> | ';
				echo '<a href="changepasswordform.php">Change Password</a> | ';
			} else {
				echo '<a href="login.php">Login</a> | ';
				echo '<a href="createuser.php">Create User</a> | ';
			}
		?>
		<a href="admin.php">Admin</a>
	</div>
</header>