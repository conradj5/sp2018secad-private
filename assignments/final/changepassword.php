<?php 
	require 'secureauthentication.php';
	include 'header.php';
	if(!isset($_POST["nocsrftoken"]) or ($_POST["nocsrftoken"] != $_SESSION["nocsrftoken"])){
		echo "<script>alert('CSR Forgery is detected')</script>";
		session_destroy();
		die();
	}
	if(!isset($_POST['pass']) or !mysql_checklogin_secure($_SESSION['username'], $_POST['pass'])){
		echo "<h1>Incorrect current password. Try again</h1>";
		die();
	}
	if(!isset($_POST['newpassword']) or !isset($_POST['confirm']) or $_POST['newpassword'] != $_POST['confirm']){
		echo "<h1>Error in new password.</h1>";
		die();
	}
	//echo "Changing password for '". $_SESSION["username"] ."' <br>";
	if (change_password($_POST['newpassword']))
		echo "<h1>Successfully updated password!</h1>";
	else
		echo "<h1>Failed to change password!</h1>";
?>