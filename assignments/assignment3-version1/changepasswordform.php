
<html>
      <h1>Change the password </h1>
<?php
  require "secureauthentication.php";
  echo "Current time: " . date("Y-m-d h:i:sa") . "<br>";
  echo "<br>Change password for '" . $_SESSION["username"] . "'<br>";
?>
          <form action="changepassword.php" method="POST" class="form login">
            <?php
              $rand = bin2hex(openssl_random_pseudo_bytes(16));
              $_SESSION["nocsrftoken"] = $rand; 
            ?>
                <input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>" />
                New Password: <input type="password" name="newpassword" /> <br>
                <button class="button" type="submit">
                  Change password
                </button>
          </form>
  </html>

