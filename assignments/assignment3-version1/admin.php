<?php 
  require 'secureauthentication.php';
?>
<h2> Welcome <?php echo htmlspecialchars($_SESSION["username"]); ?> !</h2>
<a href="logout.php">Logout</a> |
<a href="changepasswordform.php">Change password</a> |
<a href="newpost.php">New Post</a> <br>

<?php
	if(isset($_REQUEST['delete'])){
		echo "<script>if(!confirm('Are you sure you want to remove this post?')){document.location = 'admin.php';}</script>";
		remove_post($_REQUEST['delete']);
	}
	show_post_table();
?>