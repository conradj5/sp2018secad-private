<?php 
require "secureauthentication.php";

function handle_new_post(){
	$owner = $_SESSION['username'];
	$title = $_POST['title'];
	$text = $_POST['text'];
	$nocsrftoken = $_POST['nocsrftoken'];
	$sessionnocsrftoken = $_SESSION["nocsrftoken"];

	if (isset($title) and isset($text)){
		if (!isset($nocsrftoken) or ($nocsrftoken != $sessionnocsrftoken)){
			echo "cross site request forgery is detected";
			die();
		}
		if($_POST['updateid'] != '') {
			if (update_post($title, $text, $_POST['updateid'])){
				echo "Edited post";
			} else {
				echo "cannot edit post";
			}
		} else {
			if (new_post($title, $text, $owner)){
				echo "New post added";
			} else {
				echo "cannot add the new post";
			}
		}	
	}
}

$post_info = array('', '');
if(isset($_REQUEST['postid'])) {
	$post_info = get_post($_REQUEST['postid']);
}
handle_new_post();
function new_post($title, $text, $owner){
	global $mysqli;
	$prepared_sql = "INSERT into posts (title,text,owner) VALUES (?,?,?);";
	if(!$stmt = $mysqli->prepare($prepared_sql)){
		echo "Preapred Statement Error New";
	}
	$stmt->bind_param("sss", htmlspecialchars($title),
		htmlspecialchars($text),
		htmlspecialchars($owner));
    if(!$stmt->execute()) {
       echo "Execute error";
       return FALSE;
    }
	return TRUE;
}
function update_post($title, $text, $id) {
	global $mysqli;
	$prepared_sql = "UPDATE posts SET title=?, text=? WHERE id=$id;";
	if(!$stmt = $mysqli->prepare($prepared_sql)){
		echo "Preapred Statement Error Update";
	}
	$stmt->bind_param("ss", htmlspecialchars($title), htmlspecialchars($text));
    if(!$stmt->execute()) {
       echo "Execute error";
       return FALSE;
    }
	return TRUE;
}


?>

<form action="newpost.php" method="POST" class="form login">
<?php
	$rand = bin2hex(openssl_random_pseudo_bytes(16));
	$_SESSION["nocsrftoken"] = $rand;
?>
	<input type="hidden" name="updateid" value="<?php echo $_REQUEST['postid']; ?>"/>
	<input type="hidden" name="nocsrftoken" value="<?php echo $rand; ?>"/>
	Title: <input type="text" name="title" value="<?php echo $post_info[0]?>" required/><br>
	Content: <br><textarea name="text" required cols="100" rows="10"><?php echo $post_info[1]?></textarea><br>
	<button class="button" type="submit">
		Submit post
	</button>
</form>
</html>



<h2> Authenticated and active session!</h2>
<a href="admin.php">Admin page </a> | <a href="index.php">Home </a> | <a href="logout.php">Logout</a>