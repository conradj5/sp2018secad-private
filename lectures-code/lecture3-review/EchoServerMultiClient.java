import java.net.*;
import java.io.*;

public class EchoServerMultiClient {

    public static void main(String[] args) throws IOException {

        Socket clientSocket = null;

        if (args.length != 1) {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }

        int portNumber = Integer.parseInt(args[0]);
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(Integer.parseInt(args[0]));
            System.out.println("EchoServer is running on port " + Integer.parseInt(args[0]));
        } catch (IOException e) {
            System.err.println("Exception when starting server on port " + Integer.parseInt(args[0]));
        }
        while (true) {
            try {
                clientSocket = serverSocket.accept();
                System.out.println("A client is connected ");
                new EchoThread(clientSocket).start();
            } catch (IOException e) {
                System.err.println("Exception when client connecting.");
            }
        }

    }

}

class EchoThread extends Thread {

    protected Socket socket;
    protected int clientNo;
    static int clientCount = 0;


    public EchoThread(Socket clientSocket) {
        this.clientCount++;
        this.socket = clientSocket;
        this.clientNo = this.clientCount;
    }

    public void run() {
        try {
            PrintWriter out = new PrintWriter(this.socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(
                new InputStreamReader(this.socket.getInputStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                System.out.println("Received input from client #" + this.clientNo + ": " + inputLine);
                System.out.println("Echo back to client #" + this.clientNo);
                out.println(inputLine);
            }
        } catch (IOException e) {
            System.err.println("Error in client socket I/O.");
        }
    }
}

