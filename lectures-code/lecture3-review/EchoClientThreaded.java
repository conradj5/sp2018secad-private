import java.io.*;
import java.net.*;

public class EchoClientThreaded {
    public static void main(String[] args) throws IOException {
        
        if (args.length != 2) {
            System.err.println(
                "Usage: java EchoClient <host name> <port number>");
            System.exit(1);
        }

        String hostName = args[0];
        int portNumber = Integer.parseInt(args[1]);

        try {
            Socket echoSocket = new Socket(hostName, portNumber);
            System.out.println("Connected to server \'" + hostName + "\' at port "+portNumber);
            
            BufferedReader in =
                new BufferedReader(
                    new InputStreamReader(echoSocket.getInputStream()));
            new ReadThread(in, echoSocket).start();

            PrintWriter out =
                new PrintWriter(echoSocket.getOutputStream(), true);

            BufferedReader stdIn =
                new BufferedReader(
                    new InputStreamReader(System.in));
            String userInput;
            while ((userInput = stdIn.readLine()) != null) {
                out.println(userInput);
                //System.out.println("received from server: " + in.readLine());
            }
            echoSocket.close();
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host " + hostName);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for the connection to " +
                hostName);
            System.exit(1);
        } 
    }
}

class ReadThread extends Thread {
    private BufferedReader in;
    private Socket clientSocket;
    public ReadThread(BufferedReader in, Socket socket){
        this.in = in;
        this.clientSocket = socket;
    }
    public void run(){
        try {
            String inLine;
            while(!clientSocket.isClosed()) && (inLine = in.readLine()) != null)
                System.out.println("received from server: " + inLine);
            System.exit(0);
        } catch (IOException e){
            System.err.println("Couldn't get I/O");
            System.exit(1);
        }
    }
}
